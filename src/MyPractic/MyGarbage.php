<?php

/*
 * Мусорка - Получить удаленные записи для исключения из выборки некоего объекта
 */
namespace MyPractic;

class MyGarbage {

    private $obj;
    private $prefix;

    function __construct($table) {

        list($prefix, $obj) = explode('_', $table);

        $this->obj = $obj; 
        $this->prefix = $prefix;

        $this->Init();
    }

    final private function Init() {

        $q = "CREATE TABLE IF NOT EXISTS `".$this->prefix."_trash` (
  `object_id` int(11),
  `object_type` varchar(255),
  KEY `type_id` (`object_type`, `object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        \My\MyPdo::get()->query($q);
        
    }

     // "куча" удаленных объектов
    final public function Bunch() {

        $pdo = \My\MyPdo::get();

        $stmt = $pdo -> query("SELECT object_id FROM ".$this->prefix."_trash WHERE object_type = ".$pdo -> quote($this -> obj)."");
        $rows = $stmt -> fetchAll(\PDO::FETCH_COLUMN);

        return $rows;

    }

    // "положить" id в мусорку
    final public function Put($id) {

        $pdo = \My\MyPdo::get();

        $data = array('object_id' => (int) $id, 'object_type' => $this->obj);

                       $myQuery = new MyQuery($this->prefix.'_trash', $data);
        $pdo -> query( $myQuery ->Insert());
    }
}
