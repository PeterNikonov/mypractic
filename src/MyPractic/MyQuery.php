<?php
namespace MyPractic;
use My\MyPdo;

class MyQuery  {

    private $table; // таблица к которой применяем запрос
    private $garbage = array(0); // массив скрытых объектов

    private $legal = false; // существующие поля таблицы
    private $prepared = false; // подготовленные 
    private $data = false; // 

    private $orderBy; //
    private $pdo;

    public function __construct($table, $data = false) {

        $pdo = MyPdo::Get();
        $this -> table = $table;
        $this -> pdo = $pdo;
        $this -> legal = $this -> setLegal( $pdo );

        $orderField = (in_array('name', $this -> legal)) ? "name" : "id";
        $this->orderBy = " ORDER BY $orderField";

        // если прилетел массив данных - речь о вставке апдейте
        if($data) {
            
            $this -> data = $data;
            $this -> prepared = $this -> setPrepared( $pdo );

            unset($data);

        // массива нет - работаем с выборкой
        } else {

            $exp = explode('_', $table);
            // получим удаленные записи
            $garbage = new MyGarbage( $table );
            $bunch = $garbage -> Bunch();
 
            if(is_array($bunch) && count($bunch) > 0) { $this -> garbage = $bunch; }

        }
    }
    
    // условие сортировки
    public function SetOrderBy($fieldName = '') {
        if(trim($fieldName)!=='') { $this -> orderBy = 'ORDER BY '.$fieldName; }
    }

    private function setLegal( \PDO $pdo )  {

                                 // данные о структуре таблиц - часто используются, кешируем
                                 $cache_filename = "SHOW_COLUMNS_FROM ".$this -> table."";
          $cached = Cachef::read($cache_filename);
       if($cached)
       { $fields = unserialize($cached);
       } else {
           

                  $stmt = $pdo -> query("SHOW COLUMNS FROM `".$this -> table."`");
                  try {
            
                      $fields = $stmt -> fetchAll(\PDO::FETCH_COLUMN);
            Cachef::write(serialize($fields), $cache_filename);

           } catch (\Exception $ex) {
               print $ex->getMessage();
           }
       } return $fields;
    }

    private function setPrepared( $pdo ) {

        if($this -> data) {
            
            $arr = array();

            foreach($this -> data as $fieldname => $value) 
            {
                // если поле есть таблице
                if(in_array($fieldname, $this -> legal)) {

                if(is_array($value)) { throw new \Exception($fieldname. ' is array! see '.  serialize($value)); }
                if(is_object($value)) { throw new \Exception($fieldname .' is object!'.  serialize($value)); }

                $arr[$fieldname] = $pdo -> quote(trim($value));
                if($value === 'null') { $arr[$fieldname] = 'NULL'; }

                }
            } return $arr;
        }
    }

    // - U
    public function Update( $id ) {
       if($id && $this -> prepared) {
       foreach($this -> prepared as $f => $v) {
            $arr[] = " `$f` = $v ";
            }
           return "UPDATE  ".$this -> table." SET ".implode(',',$arr)." WHERE id = ". (INT) $id.";"; // что бы туда не прилетело    
        } else { return ''; }
    }
    // - C
        public function Insert() {
        if($this -> prepared) {
            foreach($this -> prepared as $f => $v) { $ins_f[] = "`$f`"; $ins_v[] = $v; }
            return  "INSERT INTO  ".$this-> table." (".implode(',',$ins_f).") VALUES (".implode(',',$ins_v).");";
        } else { return ''; }
    }

    // получить записи удовлетворяющие условиям
    public function getListByClause($stringDSN) {

        $pdo = $this->pdo;

                $e = explode(',', $stringDSN);
        foreach($e as $c) {

            $ce = explode(':', $c);
            $ca[] = "".$ce[0]." = ".$pdo -> quote(trim($ce[1])).""; 
        }

        return "SELECT * FROM  ".$this -> table." WHERE id NOT IN (".implode(',',  $this-> garbage ).") AND ".implode('AND ', $ca)." ".$this -> orderBy;

    }

    public function deleteByClause($stringDSN) {

        $pdo = $this->pdo;

                $e = explode(',', $stringDSN);
        foreach($e as $c) {

            $ce = explode(':', $c);
            $ca[] = "".$ce[0]." = ".$pdo -> quote($ce[1]).""; 
        }

        return "DELETE FROM  ".$this -> table." WHERE ".implode('AND ', $ca)."".$this -> orderBy;

    }

    // получить записи по списку идентификторов
    public function getListById($idArray, $fields = '*') {

        if(!is_array($idArray) && is_numeric($idArray)) { $idArray = array($idArray); }

                // исключим  пересечения с удаленными записями
                $legalId = @array_diff( $idArray, $this -> garbage );
             if($legalId) {
        foreach($legalId as $id) { $idin[] = (int) $id; }

                 return "SELECT $fields FROM  ".$this -> table." WHERE id IN (".implode(',', $idin ).") ".$this -> orderBy;

        } else { return ""; }
    }

    public function getFirstLetters($field = 'name') {
        return "SELECT LEFT( ".$field.", 1 ) AS letter FROM  ".$this -> table." WHERE id NOT IN (".implode(',',  $this -> garbage ).") GROUP BY letter";
    }

    public function getList($fields = '*') {
        return "SELECT $fields FROM  ".$this -> table." WHERE id NOT IN (".implode(',',  $this -> garbage ).") ".$this -> orderBy;
    }

    public function getListByLike($like) {
        return "SELECT * FROM  ".$this -> table." WHERE $like AND id NOT IN (".implode(',',  $this -> garbage ).") ".$this -> orderBy;
    }
}